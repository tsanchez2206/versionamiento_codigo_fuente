#include <stdio.h>

typedef struct Point {
   float x;
   float y;
   float z;
} Point;

float calcular(Point p1, Point p2);

Point punto_medio(Point p1, Point p2);
