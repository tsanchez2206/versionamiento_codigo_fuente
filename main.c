#include <stdio.h>

#include "point.h"

#define MAX 127

void main()
{
	//double p1x,p1y,p1z,p2x,p2y,p2z;
	Point p1, p2;
	Point pm;

	printf("Ingrese punto 1 :\n");
	printf("\tvalor x: ");
	scanf("%f", &p1.x);
	printf("\tvalor y: ");
	scanf("%f", &p1.y);
	printf("\tvalor z: ");
	scanf("%f", &p1.z);

	printf("Ingrese punto 2 :\n");
	printf("\tvalor x: ");
	scanf("%f", &p2.x);
	printf("\tvalor y: ");
	scanf("%f", &p2.y);
	printf("\tvalor z: ");
	scanf("%f", &p2.z);

	printf("Punto 1(%.2f, %.2f, %.2f)\n", p1.x, p1.y, p1.z);
	printf("Punto 2(%.2f, %.2f, %.2f)\n", p2.x, p2.y, p2.z);
	
	printf("Distancia Euclidiana: %f\n", calcular(p1, p2));

	pm = punto_medio(p1,p2);
	printf("Coordenadas del punto medio: ");
	printf("(%.2f, %.2f, %.2f)\n", pm.x, pm.y, pm.z);

}
