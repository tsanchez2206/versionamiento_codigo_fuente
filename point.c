#include <math.h>
#include "point.h"

float calcular(Point p1, Point p2){
	return(sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2)+pow(p2.z-p1.z,2)));
}

Point punto_medio(Point p1, Point p2){
	Point pm;
	pm.x = (p1.x + p2.x) / 2.0;
	pm.y = (p1.y + p2.y) / 2.0;
	pm.z = (p1.z + p2.z) / 2.0;
	return pm;
}
