CC=gcc
CFLAGS=-c -I./includes
DEPS=point.h

all: point

point: point.o main.o
	$(CC) -o point point.o main.o -I./includes -lm

%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm *o point
